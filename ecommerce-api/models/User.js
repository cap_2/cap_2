const mongoose = require("mongoose");
const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, "First name is required."]
		},
		lastName: {
			type: String,
			reqiured: [true, "Last name is required."]
		},
		email: {
			type: String,
			required: [true, "Email is required."]
		},
		password: {
			type: String,
			required: [true, "Password is required."]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		mobileNum: {
			type: String,
			required: [true, "Mobile number is required."]
		},
		orders: [
			{
				productId: {
					type: String,
					required: [true, "Order ID is required."]
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: "Ordered"
				}
			}
		]
	}
);

module.exports = mongoose.model("User", userSchema);
// 				{
// 					productId: {
// 						type: String,
// 						required: [true, "Product ID is required."]
// 					},
// 					purchasedOn: {
// 						type: Date,
// 						default: new Date()
// 					},
// 					status: {
// 						type: String,
// 						default: "Ordered"
// 					}
// 				}
// 			]
// 		// orders: [
// 		// 	{
// 		// 		orderId: {
// 		// 			type: String,
// 		// 			required: [true, "Order ID is required."]
// 		// 		},
// 		// 		purchasedOn: {
// 		// 			type: Date,
// 		// 			default: new Date()
// 		// 		},
// 		// 		price: {
// 		// 			type: Number,
// 		// 			default: "Price"
// 		// 		},
// 		// 		status: {
// 		// 			type: String,
// 		// 			default: "Ordered"
// 		// 		}
// 		// 	}
// 		// ]
// 	}
// );

// module.exports = mongoose.model("User", userSchema);