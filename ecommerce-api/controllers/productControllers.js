
const Product = require("./../models/Products")

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then( result => {
		
		return result
	})
}
module.exports.getAllProducts = () => {
	return Product.find().then( result => {
		return result
	})
}
module.exports.addProduct = (reqBody) => {
	// console.log(reqBody)

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then( (product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}
module.exports.getProduct = (params) => {

	return Product.findById(params.productId).then( product => {

		return product
	})

}
module.exports.editProduct = (params, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(params, updatedProduct, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}
module.exports.archiveProduct = (params) => {

	let updatedActiveProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params, updatedActiveProduct, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

// module.exports.getAllOrders = () => {
// 	return Product.find().then( result => {
// 		return result
// 	})
// }

// module.exports.getOrder = (params) => {
// ({"orderProducts" : {"$in": orderProducts._id}})
// 	return Product.findById(params.productId).then( product => {
	// ({"orderProducts.[]": {"$in" : _id}})
// 		return product
// 	})

// }
