
const User = require("./../models/User");
const Product = require("./../models/Products");
const bcrypt = require("bcrypt");
const auth = require("./../auth");

module.exports.checkEmailExists = (reqBody) => {
	// console.log(reqBody)
	//model.method
	return User.find({email: reqBody.email})
	.then( (result) => {
		if(result.length != 0){ 
			return true
		} else {
			return false
		}
	})
}
module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNum: reqBody.mobileNum,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then( (result, error) =>{
		if(error){
			return error
		} else {
			return true
		}
	})
} 
module.exports.login = (reqBody) => { 
	//Model.method
	return User.findOne({email: reqBody.email}).then( (result) => {

		if(result == null){
			return false

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) //boolean return

			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {
	console.log(data)
	//Model.method
	return User.findById(data).then( result => {

		result.password = "******"
		return result
	})
}

module.exports.editUser = (params) => {

	let updatedUser = {
		isAdmin: true
	}
	//Model.method
	return User.findByIdAndUpdate(params, updatedUser, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

module.exports.checkout = async (data) => {

	//save user enrollments
	const userSaveStatus = await User.findById(data.userId).then( user => {
		// console.log(user)
		user.orders.push({productId: data.productId})

		return user.save().then( (user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	}) 

		const orderSaveStatus = await Product.findById(data.productId).then( order => {
			order.orderProducts.push({userId: data.userId})
			return order.save().then( (order, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
		})


		if(userSaveStatus && orderSaveStatus){
			return true
		} else {
			return false
		}
	}

module.exports.getAllOrders = () => {
	return User.find({isAdmin: false}).then(users => {
		let allOrders = [];
		users.forEach(user => {
			allOrders.push({
				email: user.email,
				userId: user._id,
				orders: user.orders
			});
		})
		return allOrders;
	})
}