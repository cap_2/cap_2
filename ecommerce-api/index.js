const express = require("express");
const mongoose = require("mongoose");
const PORT = 3000;
const app = express();
const cors = require("cors");

let userRoutes = require("./routes/userRoutes");
let productRoutes = require("./routes/productRoutes");
// let orderRoutes = require("./routes/orderRoutes");

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

mongoose.connect("mongodb+srv://chardrichdev:Hayahay2023Chadiks@cluster0.irgko.mongodb.net/ecommerce?retryWrites=true&w=majority",
	{useNewUrlParser: true, useUnifiedTopology: true}
).then(()=>console.log(`Connected to DB`))
.catch((error)=> console.log(error))

app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
// app.use("/api/orders", orderRoutes);

app.listen(PORT,()=> console.log(`Server running at port ${PORT}`));
