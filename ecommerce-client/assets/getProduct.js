console.log(window.location.search); //?courseId=6131b2f45528f7bcdda85a56
let params = new URLSearchParams(window.location.search); 
//method of URLSearchParams
	//URLSearchParams.get()
let productId = params.get('productId');

let productName = document.querySelector('#productName');
let productDesc = document.querySelector('#productDesc');
let productPrice = document.querySelector('#productPrice');
let productContainer = document.querySelector('#productContainer');

let token = localStorage.getItem('token');

fetch(`http://localhost:3000/api/products/${productId}`,
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then( result => result.json())
.then( result => {
	console.log(result)
	// console.log(result)	//sing course document/object
    // to show course details in the web page 
	productName.innerHTML = result.name
	productDesc.innerHTML = result.description
	productPrice.innerHTML = result.price
	productContainer.innerHTML =
	`
		<button id="addCartButton" class="btn btn-primary btn-block">Add Cart</button>
	`
	let addCartButton = document.querySelector('#addCartButton');

	addCartButton.addEventListener("click", () => {

		fetch(`http://localhost:3000/api/users/checkout`,
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					productId: productId	
				})
			} 
		)
		.then( result => result.json())
		.then( result => {
			console.log(result)

			if(result){
				alert("Added to Cart")

				window.location.replace('./products.html')
				
			} else {
				alert ("Something went wrong")
			}
		})
	})
})
