
let editForm = document.querySelector('#editProduct');

let params = new URLSearchParams(window.location.search);
let productId = params.get('productId');
console.log(productId);

let token = localStorage.getItem('token');

let productName = document.querySelector('#productName');
let productPrice = document.querySelector('#productPrice');
let productDesc = document.querySelector('#productDesc');

fetch(`http://localhost:3000/api/products/${productId}`,
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {


	productName.value = result.name
	productPrice.value = result.price
	productDesc.value = result.description
})
alert('test')
editForm.addEventListener("submit", (e) => {
	e.preventDefault()

	productName = productName.value
	productDesc = productDesc.value
	productPrice = productPrice.value

	fetch(`http://localhost:3000/api/products/${productId}/edit`,
		{
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDesc,
				price: productPrice
			})
		} 
	)
	.then(result => result.json())
	.then( result => {
		
		if(result !== "undefined"){
			alert(`Product Succesfully Updated!`)

			window.location.replace('./products.html')
		} else {
			alert("Something went wrong")
		}
	})
})
