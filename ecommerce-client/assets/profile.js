let token = localStorage.getItem('token')

let name = document.querySelector('#userName');
let email = document.querySelector('#email');
let mobileNum = document.querySelector('#mobileNum');
let editButton = document.querySelector('#editButton');
let productContainer = document.querySelector('#productContainer')

fetch('http://localhost:3000/api/users/details',
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {
	console.log(result)

		name.innerHTML = `${result.firstName} ${result.lastName}`
		email.innerHTML = result.email
		mobileNum.innerHTML = result.mobileNum
		editButton.innerHTML =
		`
			<div class="mb-2">
				<a href="./editProfile.html" class="btn btn-primary">Edit Profile</a>
			</div>
		`

		result.orders.forEach((order)=>{
			console.log(order)

		// let totalAmount = order.totalAmount
		let productId = order.productId

		fetch(`http://localhost:3000/api/products/${productId}`,
			{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			}
		)
		.then( result => result.json())
		.then( result => {
			console.log(result)
			console.log(order.purchasedOn)
			let card = 
					`
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${result.name}</h5>
								<p class="card-text">${result.description}</p>
								<p class="card-text">${result.price}</p>
								<p class="card-text">${order.purchasedOn}</p>
							</div>
						</div>
					`

					productContainer.insertAdjacentHTML("beforeend", card);
		})
	})
})